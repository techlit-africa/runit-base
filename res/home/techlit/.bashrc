export PATH="/usr/src/techlit/bin:$HOME/bin:$PATH"

export EDITOR=vim

alias r=ranger

alias v=vim
alias vv='v ~/.vimrc'
alias bb='. ~/.bashrc'
alias vb='vim ~/.bashrc; bb'

alias ls='ls -h --color=always'
alias l=ls
alias la='l -a'
alias ll='la -l'

alias gs='git status'
alias gl='git log'
alias gd='git diff'
alias gba='git branch -a'
alias gco='git checkout'

alias yayaur='yay --builddir=/usr/src/aur'
alias yaytl='yay --builddir=/usr/src/techlit'

[[ -f ~/.qwer ]] && cd $(cat ~/.qwer)
qwer() { pwd > ~/.qwer; }

vw() { v "$(which $1)"; }

export NVM_DIR=~/.nvm
[[ -s ~/.nvm/nvm.sh ]] && source ~/.nvm/nvm.sh
[[ -s ~/.nvm/bash_completion ]] && source ~/.nvm/bash_completion
