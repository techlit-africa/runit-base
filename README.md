provision a runit-based artix for older techlit machines

ISSUES
1. lightdm multiple monitors are not arranged sensibly (no overlap, same dpi)

RESOURCES
https://ubuntu-mate.community/t/how-to-tweak-your-panel-layouts/884
https://forums.linuxmint.com/viewtopic.php?t=327379
`/usr/share/mate-{panel,*}`
